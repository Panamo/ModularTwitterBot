# Create a module for the bot

If you want to create a module for the bot, you first have to add a compiled
version of this code as a dependency to your project. Then you have to create a 
class, which extends from the class "Module" which is in the bot. The class 
contains 3 empty methods, that you can override to implement features. The
methods are called "onLoad", "onEnable" and "onDisable". The Module-Class also 
contains a Configuration where you can save things in. You can access it through 
"getConfiguration". You can use the important things from the bot with an
instance of "ModularTwitterBot" which you can get using 
"ModularTwitterBot.getInstance()". There you can access for example the 
CommandManager, where you can add and execute commands.

Also important is that every module needs a "module.yml". There you have to add
the fields "main" for the Path to the class where you extended from the Module-
Class, "version" for the current version of the Module, "name" for the name of 
the module and "author" for the Author of the Module. These fields have to be 
added in the YAML-format.


# Add the bot dependency through maven

Because I got no Maven-Repository for the bot, you have to install the bot 
dependency on your PC yourself. You just have to download the source, navigate
in the directory with your shell and execute "mvn install". Then you can add 
this dependency to your POM:

    <dependency>
        <groupId>de.panamo</groupId>
        <artifactId>modulartwitterbot</artifactId>
        <version>1.0-SNAPSHOT</version>
    </dependency>