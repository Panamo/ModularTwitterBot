package de.panamo.modulartwitterbot.module;

import de.panamo.modulartwitterbot.Configuration;
import de.panamo.modulartwitterbot.ModularTwitterBot;
import java.io.File;

public class Module {
    private ModuleInfo moduleInfo;
    private Configuration configuration;
    private boolean enabled;

    public Module() { }

    void init(ModuleInfo moduleInfo) {
        this.moduleInfo = moduleInfo;
        File moduleFolder = new File(ModularTwitterBot.getInstance()
                .getModuleManager().getModulesFolder(), moduleInfo.getName());
        moduleFolder.mkdir();
        this.configuration = new Configuration(new File(moduleFolder, "config.yml"));
    }

    public void onLoad() { }
    public void onEnable() { }
    public void onDisable() { }

    public ModuleInfo getModuleInfo() {
        return moduleInfo;
    }

    public Configuration getConfiguration() {
        return configuration;
    }

    public boolean isEnabled() {
        return enabled;
    }

    void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }
}
