package de.panamo.modulartwitterbot.module;

import de.panamo.modulartwitterbot.ModularTwitterBot;
import org.yaml.snakeyaml.Yaml;
import org.yaml.snakeyaml.constructor.Constructor;
import org.yaml.snakeyaml.introspector.PropertyUtils;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

public class ModuleManager {
    private ModularTwitterBot instance;
    private Map<String, Module> modules = new ConcurrentHashMap<>();
    private Yaml yaml;
    private File modulesFolder;

    public ModuleManager(ModularTwitterBot instance, File modulesFolder) {
        this.instance = instance;
        this.modulesFolder = modulesFolder;

        if(!modulesFolder.exists())
            modulesFolder.mkdir();

        Constructor yamlConstructor = new Constructor();
        PropertyUtils propertyUtils = yamlConstructor.getPropertyUtils();
        propertyUtils.setSkipMissingProperties(true);
        yamlConstructor.setPropertyUtils(propertyUtils);
        this.yaml = new Yaml(yamlConstructor);
    }

    public void loadModules() {
        if(this.modulesFolder.exists()) {
            for (File file : Objects.requireNonNull(this.modulesFolder.listFiles())) {
                if(file.getName().endsWith(".jar")) {
                    try(JarFile jarFile = new JarFile(file)) {
                        JarEntry moduleInfoEntry = jarFile.getJarEntry("module.yml");
                        if(moduleInfoEntry == null)
                            throw new NullPointerException("No module.yml found in " + file.getName());


                        InputStream inputStream = jarFile.getInputStream(moduleInfoEntry);
                        ModuleInfo moduleInfo = this.yaml.loadAs(inputStream, ModuleInfo.class);
                        if(moduleInfo.getMain() == null)
                            throw new NullPointerException("Main class is missing in " + file.getName());


                        URLClassLoader classLoader = new URLClassLoader(new URL[]{file.toURI().toURL()});
                        Class<?> moduleClass = classLoader.loadClass(moduleInfo.getMain());
                        Module module = (Module) moduleClass.newInstance();
                        module.init(moduleInfo);
                        this.modules.put(moduleInfo.getName(), module);
                        module.onLoad();

                        this.instance.getBotLogger().info("Successfully loaded module " + moduleInfo.getName()
                                + " version "+moduleInfo.getVersion() + " by " + moduleInfo.getAuthor());
                    } catch (IOException | ClassNotFoundException | InstantiationException | IllegalAccessException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    public void enableModule(Module module) {
        if(!module.isEnabled()) {
            this.instance.getBotLogger().info("Enabling module " + module.getModuleInfo().getName() + " ...");
            module.setEnabled(true);
            module.onEnable();
        }
    }

    public void enableModules() {
        for(Module module : this.modules.values())
            this.enableModule(module);
    }

    public void disableModule(Module module) {
        if(module.isEnabled()) {
            module.setEnabled(false);
            module.onDisable();
            this.instance.getBotLogger().info("Disabling module " + module.getModuleInfo().getName() + " ...");
        }
    }

    public void disableModules() {
        for(Module module : this.modules.values())
            this.disableModule(module);
    }

    public Map<String, Module> getModules() {
        return modules;
    }

    public File getModulesFolder() {
        return modulesFolder;
    }
}
