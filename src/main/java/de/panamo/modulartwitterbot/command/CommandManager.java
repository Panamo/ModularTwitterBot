package de.panamo.modulartwitterbot.command;


import de.panamo.modulartwitterbot.ModularTwitterBot;
import de.panamo.modulartwitterbot.console.ConsoleHandler;
import de.panamo.modulartwitterbot.module.Module;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Level;
import java.util.logging.LogRecord;

public class CommandManager implements ConsoleHandler {
    private Map<String, Command> commands = new ConcurrentHashMap<>();

    public void registerCommand(Module module, Command command) {
        command.setModule(module);
        this.commands.put(command.getName().toLowerCase(), command);
        for(String alias : command.getAliases())
            this.commands.put(alias.toLowerCase(), command);
    }

    public void unregisterCommand(String commandName) {
        commandName = commandName.toLowerCase();
        Command command = this.commands.get(commandName);
        this.commands.remove(commandName);
        for(String alias : command.getAliases())
            this.commands.remove(alias.toLowerCase());
    }

    public void unregisterCommands(Module module) {
        for(Command command : this.commands.values())
            if(command.getModule().equals(module))
                this.unregisterCommand(command.getName());
    }

    @Override
    public void handleInput(String consoleLine) {
        String commandName;
        if(consoleLine.contains(" "))
            commandName = consoleLine.split(" ")[0].toLowerCase();
        else
            commandName = consoleLine.toLowerCase();

        if(this.commands.containsKey(commandName)) {
            Command command = this.commands.get(commandName);
            String rawArgs = consoleLine.substring(commandName.length()).trim();
            String[] args;
            if(rawArgs.contains(""))
                args = rawArgs.split(" ");
            else
                args = new String[]{rawArgs};

            try {
                command.execute(args);
            } catch (Throwable throwable) {
                ModularTwitterBot.getInstance().getBotLogger().logError(throwable,
                        "An error occurred while executing command " + commandName);
            }
        } else
            ModularTwitterBot.getInstance().getBotLogger().info("Unknown command. Use 'help' for help");
    }

    public Set<Command> getCommands() {
        return new HashSet<>(this.commands.values());
    }
}
