package de.panamo.modulartwitterbot.console;

import de.panamo.modulartwitterbot.logger.BotLogger;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.logging.Level;

public class LogFormatPrintStream extends PrintStream {
    private BotLogger logger;
    private Level logLevel;

    public LogFormatPrintStream(OutputStream out, BotLogger logger, Level logLevel) {
        super(out);
        this.logger = logger;
        this.logLevel = logLevel;
    }

    @Override
    public void print(boolean b) {
        this.logger.log(this.logLevel, b ? "true" : "false");
    }

    @Override
    public void print(int i) {
        this.logger.log(this.logLevel, String.valueOf(i));
    }

    @Override
    public void print(char c) {
        this.logger.log(this.logLevel, String.valueOf(c));
    }

    @Override
    public void print(long l) {
        this.logger.log(this.logLevel, String.valueOf(l));
    }

    @Override
    public void print(float f) {
        this.logger.log(this.logLevel, String.valueOf(f));
    }

    @Override
    public void print(double d) {
        this.logger.log(this.logLevel, String.valueOf(d));
    }

    @Override
    public void print(char[] s) {
        this.logger.log(this.logLevel, String.valueOf(s));
    }

    @Override
    public void print(String s) {
        if(s == null)
            s = "null";
        this.logger.log(this.logLevel, s);
    }

    @Override
    public void print(Object obj) {
        this.logger.log(this.logLevel, String.valueOf(obj));
    }
}
