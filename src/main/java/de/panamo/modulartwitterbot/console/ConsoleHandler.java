package de.panamo.modulartwitterbot.console;


public interface ConsoleHandler {

    void handleInput(String consoleLine);

}
