package de.panamo.modulartwitterbot.logger;

import de.panamo.modulartwitterbot.console.Console;
import de.panamo.modulartwitterbot.console.LogFormatPrintStream;
import java.nio.charset.StandardCharsets;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;

public class BotLogger extends Logger {

    public BotLogger(Console console) {
        super("BotLogger", null);

        BotLoggerHandler botLoggerHandler = new BotLoggerHandler(console);
        botLoggerHandler.setFormatter(new BotLogFormatter());
        botLoggerHandler.setLevel(Level.INFO);
        botLoggerHandler.setEncoding(StandardCharsets.UTF_8.name());
        super.addHandler(botLoggerHandler);

        System.setOut(new LogFormatPrintStream(System.out, this, Level.INFO));
        System.setErr(new LogFormatPrintStream(System.err, this, Level.SEVERE));
    }

    public void logError(Throwable error, String message) {
        LogRecord logRecord = new LogRecord(Level.SEVERE, message);
        logRecord.setThrown(error);
        this.log(logRecord);
    }
}
