package de.panamo.modulartwitterbot.logger;

import de.panamo.modulartwitterbot.console.Console;
import java.io.UnsupportedEncodingException;
import java.util.logging.Handler;
import java.util.logging.LogRecord;

public class BotLoggerHandler extends Handler {
    private Console console;

    BotLoggerHandler(Console console) {
        this.console = console;
    }

    @Override
    public void publish(LogRecord record) {
        if (super.isLoggable(record))
            this.console.println(super.getFormatter().format(record));
    }

    @Override
    public synchronized void setEncoding(String encoding) {
        try {
            super.setEncoding(encoding);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void flush() { }

    @Override
    public void close() throws SecurityException { }
}
